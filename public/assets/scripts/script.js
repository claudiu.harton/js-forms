const app = {
    registerApi: '/api/participants',

    //initial values
    trainingOptions: ['front-end', 'back-end'],
    reasonsForComing: ['Because I want to learn', 'Because people are nice'],

    //selectors
    trainingOptionSelector: document.querySelector('#trainingOption'),
    reasonsForComingSelector: document.querySelector('#reasonsForComing'),
    customReasonSelector: document.querySelector('#customReason'),
    customReasonFormGroupSelector: document.querySelector('#customReasonFormGroup'),
    customReasonButtonSelector: document.querySelector('#customReasonButton'),
    submitButtonSelector: document.querySelector('#submitButton'),
    nameSelector: document.querySelector('#nameInput'),
    emailSelector: document.querySelector('#emailInput'),
    phoneSelector: document.querySelector('#phoneInput'),
    ageSelector: document.querySelector('#ageInput'),

    //state
    isCustomReasonInputDisplayed: 0,

    //functions for generate fields
    generateTrainingOptions: () => {
        let html = '';

        app.trainingOptions.forEach((item, index) => {
            html += `<div class="form-check">
                        <input class="form-check-input" type="radio" name="trainingOption" id="trainingOption${index}Input"
                              value="${item}">
                        <label class="form-check-label" for="trainingOption${index}Input">
                            ${app.capitalize(item)}
                        </label>
                     </div>`
        });

        app.trainingOptionSelector.innerHTML = html;
    },
    generateReasonsForComing: () => {
        let html = '';
        app.reasonsForComing.forEach((item, index) => {
            html += ` <div class="form-check">
                           <input class="form-check-input" type="checkbox" name="reasonsForComing" value="${app.capitalize(item)}"
                                 id="reason${index}Input">
                           <label class="form-check-label" for="reason${index}Input">
                             ${app.capitalize(item)}
                           </label>
                      </div>`
        });
        app.reasonsForComingSelector.innerHTML = html;
    },
    addReasonForComing: (customReason) => {
        const index = app.reasonsForComing.length - 1
        let html = ` <div class="form-check">
                           <input class="form-check-input" type="checkbox" name="reasonsForComing" value="${app.capitalize(customReason)}"
                                 id="reason${index}Input">
                           <label class="form-check-label" for="reason${index}Input">
                             ${app.capitalize(customReason)}
                           </label>
                      </div>`
        app.reasonsForComingSelector.innerHTML += html;
    },

    addClickEventListenerForCustomReasonButton: () => {
        app.customReasonButtonSelector.addEventListener('click', () => {
            toastr.remove();
            if (!app.isCustomReasonInputDisplayed) {
                app.customReasonButtonSelector.innerHTML = "Submit custom reason";
                app.show(app.customReasonFormGroupSelector);
                app.disable(app.submitButtonSelector);
                app.isCustomReasonInputDisplayed = !app.isCustomReasonInputDisplayed
            } else {
                const customReason = app.customReasonSelector.value;

                if (app.validateCustomReason(customReason)) {
                    app.reasonsForComing.push(customReason);
                    app.addReasonForComing(customReason);
                    app.customReasonButtonSelector.innerHTML = "Add custom reason";
                    app.customReasonSelector.value = '';
                    app.hide(app.customReasonFormGroupSelector);
                    app.enable(app.submitButtonSelector);
                    app.isCustomReasonInputDisplayed = !app.isCustomReasonInputDisplayed
                } else toastr.error('Empty custom reason field')
            }


        }, false)
    },
    addClickEventListenerForSubmitButton: () => {
        app.submitButtonSelector.addEventListener('click', () => {
            toastr.remove();

            const participant = app.getParticipant();
            if (app.validateParticipant(participant)) {
                axios.post(`${app.registerApi}`, {...participant, age: Number(participant.age)})
                    .then(response => {
                        toastr.success(response.data.message);
                        app.emptyForm();
                    })
                    .catch(error => {
                        switch (error.response.status) {
                            case 400:
                                Object.values(error.response.data.errors).forEach(item => {
                                    toastr.error(item)
                                })
                                break;
                            case 500:
                                toastr.error(error.response.data.message);
                                break;
                        }
                    })
            }
        }, false)
    },
    validateCustomReason: (customReason) => {
        return customReason;
    },
    validateParticipant: (participant) => {
        let canBeSaved = true;

        if (!participant.reasonsForComing || participant.reasonsForComing.length === 0) {
            canBeSaved = false;
            toastr.error("No reason for coming was provided")
        }

        if (!participant.trainingOption) {
            canBeSaved = false;
            toastr.error("No training option was provided")
        } else if (!app.trainingOptions.includes(participant.trainingOption)) {
            canBeSaved = false;
            toastr.error("Invalid training option")
        }

        if (participant.age && !participant.age.toString().match(/^[0-9]*$/)) {
            canBeSaved = false;
            toastr.error("Invalid age")
        }

        if (!participant.phone) {
            canBeSaved = false;
            toastr.error("No phone was provided")
        } else if (!participant.phone.match(/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/)) {
            canBeSaved = false;
            toastr.error("Invalid phone")
        }

        if (!participant.email) {
            canBeSaved = false;
            toastr.error("No email was provided")
        } else if (!participant.email.match(/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/)) {
            canBeSaved = false;
            toastr.error("Invalid email")
        }

        if (!participant.name) {
            canBeSaved = false;
            toastr.error("No name was provided");
        } else if (!participant.name.match(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)) {
            canBeSaved = false;
            toastr.error("Invalid name")
        }

        return canBeSaved;
    },
    getParticipant: () => {
        const trainingOptionsInputs = app.multipleInputsSelector('trainingOption');
        const reasonsForComingInputs = app.multipleInputsSelector('reasonsForComing');

        return {
            name: app.nameSelector.value,
            email: app.emailSelector.value,
            phone: app.phoneSelector.value,
            age: app.ageSelector.value,
            trainingOption: app.getValueFromRadioInput(trainingOptionsInputs),
            reasonsForComing: app.getValuesFromCheckBoxInput(reasonsForComingInputs),
        }
    },

    //util functions
    capitalize: (text) => {
        if (typeof text !== 'string') return '';
        return text.charAt(0).toUpperCase() + text.slice(1)
    },
    show: (input) => {
        input.setAttribute('style', ' display: inherit;');
    },
    hide: (input) => {
        input.setAttribute('style', ' display: none;');
    },
    disable: (input) => {
        input.disabled = true;
    },
    enable: (input) => {
        input.disabled = false;
    },
    getValueFromRadioInput: (inputList) => {
        let value = '';
        inputList.forEach(item => {
            if (item.checked)
                value = item.value
        });
        return value;
    },
    getValuesFromCheckBoxInput: (inputList) => {
        const valueList = [];
        inputList.forEach(item => {
            if (item.checked)
                valueList.push(item.value)
        });
        return valueList;
    },
    uncheckMultipleInputs: (inputList) => {
        inputList.forEach(item => {
            item.checked = false
        })
    },
    multipleInputsSelector: (name) => document.querySelectorAll(`input[name=${name}]`),
    emptyForm: () => {
        const trainingOptionsInputs = app.multipleInputsSelector('trainingOption');
        const reasonsForComingInputs = app.multipleInputsSelector('reasonsForComing');
        app.nameSelector.value = '';
        app.emailSelector.value = '';
        app.phoneSelector.value = '';
        app.ageSelector.value = '';
        app.uncheckMultipleInputs(trainingOptionsInputs);
        app.uncheckMultipleInputs(reasonsForComingInputs);

    },

    init: () => {
        app.generateTrainingOptions();
        app.generateReasonsForComing();
        app.addClickEventListenerForCustomReasonButton();
        app.addClickEventListenerForSubmitButton();

    }
};

app.init();