# JS training API

Presequels:
* NodeJS ([Homepage - Download the LTS version](https://nodejs.org/en/))
* Git ([Homepage](https://git-scm.com/))

execute:
1. ``git clone https://gitlab.com/claudiu.harton/js-forms.git``
1. ``cd js-forms``
1. ``git checkout ch0``
1. ``npm install``
1. ``npm start``

## Ch0 - ce avem de facut?
- Putem vedea cum putem trimite datele intrand pe pagina de documentarii (http://localhost:8080/docs/) si meniul se poate vedea ce date sunt obligatorii si ce fel de date ar trebui sa contina. Pe langa asta se mai poate vedea si ce response putem avea, pentru a trata fiecare in parte.
- Se poate vedea ca pentru fiecare element din structura request-ului avem cate un element in pagina principala(http://localhost:8080/). Cate un input pentru **name**, **email**, **phone** si **age**, iar pe langa astea, un set de radio inputs pentru **trainingOption** si un set de checkbox-uri pentru **reasonsForComing**. In urmatoarele challenge-uri veti realiza preluarea datelor dinaceste formulare, le veti valida, in caz de sunt eronate sa anuntati utilizatorul, iar daca e totul in regula sa trimiteti datele catre server. Folderul in care se va lucra va fi cel numit **public**.

## Ch1 – inițializarea formularului 
- Întrucât avem deja în HTML creat formularul, nu vom intra in acest fisier, si vom incepe direct în cadrul fișierului **script.js**.
*Ne vom folosi de conceptul de încapsulare, iar formularul nostru va fi un obiect, declarat cu ajutorul cuvântului cheie const, ce sugerează faptul că structura acestuia nu e mutabilă.*
- Vom crea si intializa o variabila constanta numita **app** in care pune o proprietate numita **init** ce va avea asociata o functie ce nu primeste niciun parametru ci doar va printa un mesaj in consola *"The form was initiated"*. 
- Dupa ce ai facut asta, apeleaza functia imediat dupa obiectul definit *app.init();* si apoi dupa ce ai salvat, intra in browser si verifica daca apare in consola.

#### [1] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch1/public/assets/scripts/script.js))

## Ch2 – selectori pentru elementele din formular
- Avand in vedere ca elementele din pagina sunt obiecte, ele pot fi accesate si folosind javascript. Petru a face acest lucru ne vom folosi de diferitele moduri de a selecta un anumit element, metoda destul de folosita in CSS pentru a aplica stiluri doar unor elemente.
- Un mod destul de comun de a lua referinta elementului din pagina si a o pune intr-o variabila este _querySelector(selector)_. querySelector() nu are restricții în ceea ce privește argumentul dat, deci vom folosi id-urile atribuite câmpurilor din formular.
si pentru moment vom adauga cate o proprietate ce vor avea asociate referinte catre elementele **#nameInput**, **#nameInput**, **#phoneInput**, **#ageInput**, **#reasonsForComing**, **#trainingOption**, **#submitButton**.
- Poti testa acesti selectori apaland in consola browser-ului: ``console.log(app.nameSelector)``

#### [2] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch2/public/assets/scripts/script.js))

## Ch3 – realizare funcție pentru a prelua informațiile unui participant
- Acum că avem acces la câmpuri, ne dorim să putem vedea și datele introduse în formular. Pentru asta vom defini o metodă ce va returna valorile găsite în câmpuri, folosind proprietatea unui element: **value**.
- cream o functie numita **getParticipant** ce va extrage folosind selectorii catre elemente din DOM, **numele**, **emailul**, **telefonul** si **varsta** si va return un obiect cu cate un atribut pentru fiecare din acestea.
- La fel ca celelalte, puteti testa in browser daca va printeaza ce ar trebui.

#### [3] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch3/public/assets/scripts/script.js))

## Ch4 – crearea unei metode pentru a putea salva referintele elementelor de tip radio/checkbox
- Practic vom crea o noua proprietate **multipleInputsSelector** pentru care vom atasa o functie ce primeste ca parametru un string **name** (vrem sa selectam elementele dupa proprietatea name a acestora), si returneaza rezultatul apelului functiei **document.querySelectorAll(`input[name=${name}]`)**, unde practic vom folosi un selector special pentru a lua toate elementele ce au proprietatea nume egala cu ce primeste ca parametru si sa le punem intr-o lista.

#### [4] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch4/public/assets/scripts/script.js))

## Ch5 – preluarea valorilor din radio buttons
- Declarăm o metodă numită _getValueFromRadioInput_ care va primi o listă cu referinte catre elementele de tip radio input. Declarăm o variabilă și o inițializăm cu un string gol ('').
- Parcurgând lista primită folosind o functie specifica unei variable de tip lista (**forEach**). Aceasta functie primeste ca parametru o functie in care parametrul este elementul curent si in interiorul acesteia verficam daca elementrul are proprietata **checked** true (aceasta tine valoarea booleana daca radio este bifat sau nu) astfel pentru fiecare element din ea se verifică dacă este checked sau nu. Cu alte cuvinte, verificăm dacă opțiunile sunt bifate sau nu. Avand in vedere ca o singura valoare va fi true, cand o gaseste pe aceea o va pune in variabila, iar daca nu va return stringul gol.Si la sfarsit o returnăm.

#### [5] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch5/public/assets/scripts/script.js))

## Ch6 – selectarea radio buttons
- Pentru a obține valoarea din radio buttons, ne vom folosi de metoda **multipleInputsSelector**(definită anterior), ce va returna o lista cu opțiunile posibile si o vom pune intr-o variabila noua in metoda **getParticipant**, si vom adauga o proprietate noua obiectului pe care il vom returna ce va tine si valoarea pentru **trainingOption** folosind _getValueFromRadioInput_ ce va primi ca parametru variabila ce tine lista de referinte.

#### [6] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch6/public/assets/scripts/script.js))

## Ch7 – preluarea valorilor din checkbox
- Asemănător ch5, doar că de data aceasta nu ne folosim de o variabilă simplă, ci de un **array** în care păstrăm tot ce a fost selectat de utilizator. (dupa cum se putea vedea si in documentatie)
Adăugarea valorilor în lista cu opțiunile selectate se face folosind metoda _push()_.

#### [7] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch7/public/assets/scripts/script.js))

## Ch8 – returnarea în cadrul profilului participantului a variantelor alese în radio & checkboxes
În metoda _getParticipant_ definim proprietățile _trainingOption_ și _reasonsForComing_, ce-și iau valorile prin apelarea funcțiilor definite la challenge-urile anterioare.

#### [8] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch8/public/assets/scripts/script.js))

## Ch9 – adăugare EventListener pentru butonul de submit & verificare completare câmpuri
Pentru că acum putem accesa valorile câmpurilor din formular, este timpul să adăugăm un eveniment pe butonul de submit. Înainte să facem asta, vom verifica dacă toate câmpurile au fost completate.
Astfel, creăm o metodă _validateParticipant_ ce primește ca parametru de intrare un obiect participant.
Declarăm o variabilă canBeSaved de tip boolean, pe care o considerăm inițial true. Printr-o serie de if-uri verificăm dacă există valoare în câmp, setăm valoarea variabilei _canBeSaved_ și afișăm în consolă un mesaj.
Totodată, vom apela metoda de eventListener în cadrul metodei de inițializare a formularului.
**Caz special**: în cazul checkboxului, condiția pentru invalidare va fi dacă nu există un array returnat de funcție sau dacă lungimea acestuia e 0, ceea ce e echivalent.

#### [9] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch9/public/assets/scripts/script.js))

## Ch10 – validări în ceea ce privește formatul datelor introduse
La funcțiile de validare definite anterior vom adăuga verificări în ceea ce privește formatul datelor ce au fost introduse de utilizator prin folosirea metodei _match_ ce verifică un pattern, schimbă valoarea variabilei și afișează în consolă un mesaj de eroare.

#### [10] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch10/public/assets/scripts/script.js))

## Ch11 - request către server
Acum că am validat datele, în cadrul funcției care se ocupă cu eventul de click pe butonul de submit vom adăuga trimiterea datelor, condiționată bineînțeles de validarea obiectului participant. Vom folosi **axios**, bazat pe un promise, requestul fiind unul de tipul post. Astfel, declarăm legătura cu endpoint-ul _api/participants_, printr-o proprietate. În cadrul metodei post specificăm endpointul și ceea ce trimitem, adică participantul. Răspunsul primit va fi apoi afișat în consolă.

#### [11] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch11/public/assets/scripts/script.js))

## Ch12 – Adăugare excepții la post
Pentru că există posibilitatea să fii apărut o eroare în conexiunea cu serverul, vom defini și ceea ce se întamplă în cazul erorilor **400 – Bad Request** (adică browserul a trimis ceva ce serverul nu înțelege) și **500 – Internal Server Error**.
În ambele cazuri se afișează în consolă mesajul de eroare.

#### [12] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch12/public/assets/scripts/script.js))

## Ch13 – Restabilire câmpuri formular
Odată ce formularul a fost trimis, vom declara o metodă prin care setăm ca fiind nule valorile din câmpurile din formular.

#### [13] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch13/public/assets/scripts/script.js))

## Ch14 – Creare funcție pentru deselectarea inputurilor de tip radio & checkbox
Vom seta printr-o metodă ce are ca parametru de intrare o listă cu valorile introduse ca pentru fiecare element din listă, câmpul respectiv să fie schimbat în unchecked. (adică item.checked = false)
Metoda va fii apoi apelată în metoda emptyForm atât pentru radio buttons, cât și pentru checkboxes.

#### [14] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch14/public/assets/scripts/script.js))

## Ch15 – Aplicare mesaje de eroare prin folosirea toastr
Pentru toate validările pe care le-am declarat vom folosi **toastr**, o librărie JS, pentru a afișa utilizatorului mesajele de eroare, în loc să le afișăm în consolă.

#### [15] Code snippet ([aici](https://gitlab.com/claudiu.harton/js-forms/blob/ch15/public/assets/scripts/script.js))