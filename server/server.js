const express = require('express');
const bodyParser = require('body-parser')
const swaggerJSDoc = require('swagger-jsdoc');
const pathToSwaggerUi = require('swagger-ui-dist').absolutePath();
const fs = require('fs');

const app = express();
app.use(bodyParser.json())


app.use(express.static('public'))

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'Javascript forms training API',
        version: '1.0.0',
        description: 'API for testing the forms made in frontend',
    },
    host: `localhost:8080`,
    basePath: '/',
};

const options = {
    swaggerDefinition,
    apis: ['./*.yaml'],
};

app.get('/api/docs/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerJSDoc(options));
})


app.get('/docs', (req, res) => {
    const indexContent = fs.readFileSync(`${pathToSwaggerUi}/index.html`)
        .toString()
        .replace("https://petstore.swagger.io/v2/swagger.json", `http://localhost:8080/api/docs/swagger.json`)
    res.send(indexContent)
});

app.use('/docs', express.static(pathToSwaggerUi));

app.get('/api/participants', (req, res) => {
    try {
        const participants = JSON.parse(fs.readFileSync('./db.json')).participants;
        res.status(200).send(participants)
    } catch (e) {
        console.log(e)
        res.status(500).send({message: 'Server error'})
    }
})

app.post('/api/participants', (req, res) => {
    try {
        let db = JSON.parse(fs.readFileSync('./db.json'));
        let errors = {};

        const participant = {
            id: db.participants[db.participants.length - 1].id + 1,
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            age: req.body.age,
            trainingOption: req.body.trainingOption,
            reasonsForComing: req.body.reasonsForComing
        }


        if (!participant.name) {
            errors.name = "No name was provided"
        } else if (!participant.name.match(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/))
            errors.name = "Invalid name"

        if (!participant.email) {
            errors.email = "No email was provided"
        } else if (!participant.email.match(/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/))
            errors.email = "Invalid email"
        else if (db.participants.find(item => item.email === participant.email))
            errors.email = "Email is already used"

        if (!participant.phone) {
            errors.phone = "No phone was provided"
        } else if (!participant.phone.match(/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/))
            errors.phone = "Invalid phone"

        if (!participant.trainingOption) {
            errors.trainingOption = "No training option was provided"
        } else if (!['front-end', 'back-end'].includes(participant.trainingOption))
            errors.trainingOption = "Invalid training option"

        if (!participant.reasonsForComing || participant.reasonsForComing.length === 0) {
            errors.reasonsForComing = "No reason for coming was provided"
        }

        if (participant.age && !participant.age.toString().match(/^[0-9]*$/)) {
            errors.age = "Invalid age"
        }

        if (Object.keys(errors).length === 0) {
            db.participants.push(participant)
            fs.writeFileSync('db.json', JSON.stringify(db));
            console.log(`Participant ${participant.email} had registered`)
            res.status(201).send({message: `Successful registration`})
        } else {
            res.status(400).send({errors: errors})
        }

    } catch (e) {
        console.log(e)
        res.status(500).send({message: 'Server error'})
    }
})


app.listen(8080, () => {
    console.log(`Server is running on http://localhost:8080`)
})